# TP 6

Pour lancer l'application server:

```
git clone https://gitlab.com/lpbb/tp6-conception-logicielle.git
cd tp6-conception-logicielle
cd server
pip install -r requirements.txt
uvicorn main:app --reload
```

Pour lancer l'application client:

```
git clone https://gitlab.com/lpbb/tp6-conception-logicielle.git
cd tp6-conception-logicielle
cd client
pip install -r requirements.txt
python3 main.py
```