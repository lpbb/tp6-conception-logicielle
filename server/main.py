from fastapi import FastAPI
from get_mots import getMots
from liste_mot import ListeMot
from mot import Mot
app = FastAPI()

@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/mots")
def read_mot():
    return  {"list_mots" : getMots()
    }

@app.post("/mot")
async def add_mot(word : Mot):
    ListeMot().ajout_mot(word=word)

